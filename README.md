# README #

### What is this repository for? ###

* This is my first Web Application built using python and Flask. It is a simple bucket list site that explores basic features like user signup, login, managment, user created bucket lists, editing and displaying, maintaining sessions, etc.
* 1.0.0

### How do I get set up? ###

* To run this you need to have python and Flask installed.
* Create a database in MySOL by the name 'BucketList' and import bucketlist.sql located in the root folder into this database.
* Then all you have to do is run the app.py file from the python console.
* By default, the server will run at localhost:5000, this port number can be changed in the configurations.

### Who do I talk to? ###

* You can contact me @ mail@kgopalkrishna.com