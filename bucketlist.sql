-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2017 at 02:37 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bucketlist`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_AddUpdateLikes` (`p_wish_id` INT, `p_user_id` INT, `p_like` INT)  BEGIN
    if (select exists (select 1 from tbl_likes where wish_id = p_wish_id and user_id = p_user_id)) then
 
        update tbl_likes set wish_like = p_like where wish_id = p_wish_id and user_id = p_user_id;
         
    else
         
        insert into tbl_likes(
            wish_id,
            user_id,
            wish_like
        )
        values(
            p_wish_id,
            p_user_id,
            p_like
        );
 
    end if;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_addWish` (IN `p_title` TEXT, IN `p_description` TEXT, IN `p_user_id` BIGINT, IN `p_file_path` TEXT, IN `p_is_private` INT, IN `p_is_done` INT)  BEGIN
    insert into tbl_wish(
        wish_title,
        wish_description,
        wish_user_id,
        wish_date,
        wish_file_path,
        wish_private,
        wish_accomplished
    )
    values
    (
        p_title,
        p_description,
        p_user_id,
        NOW(),
        p_file_path,
        p_is_private,
        p_is_done
    );
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_createUser` (IN `p_name` VARCHAR(40), IN `p_username` TEXT, IN `p_password` TEXT)  BEGIN
    if ( select exists (select 1 from tbl_user where user_username = p_username) ) THEN
     
        select 'Username Exists !!';
     
    ELSE
     
        insert into tbl_user
        (
            user_name,
            user_username,
            user_password
        )
        values
        (
            p_name,
            p_username,
            p_password
        );
     
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteWish` (IN `p_wish_id` BIGINT, IN `p_user_id` BIGINT)  BEGIN
delete from tbl_wish where wish_id = p_wish_id and wish_user_id = p_user_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_GetAllWishes` ()  BEGIN
    select wish_id,wish_title,wish_description,wish_file_path from tbl_wish where wish_private = 0;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_GetWishById` (IN `p_wish_id` BIGINT, IN `p_user_id` BIGINT)  BEGIN
select wish_id,wish_title,wish_description,wish_file_path,wish_private,wish_accomplished from tbl_wish where wish_id = p_wish_id and wish_user_id = p_user_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_GetWishByUser` (IN `p_user_id` BIGINT, IN `p_limit` INT, IN `p_offset` INT, OUT `p_total` BIGINT)  BEGIN
     
    select count(*) into p_total from tbl_wish where wish_user_id = p_user_id;
 
    SET @t1 = CONCAT( 'select * from tbl_wish where wish_user_id = ', p_user_id, ' order by wish_date desc limit ',p_limit,' offset ',p_offset);
    PREPARE stmt FROM @t1;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_updateWish` (IN `p_title` TEXT, IN `p_description` TEXT, IN `p_wish_id` BIGINT, IN `p_user_id` BIGINT, IN `p_file_path` TEXT, IN `p_is_private` INT, IN `p_is_done` INT)  BEGIN
update tbl_wish set
    wish_title = p_title,
    wish_description = p_description,
    wish_file_path = p_file_path,
    wish_private = p_is_private,
    wish_accomplished = p_is_done
    where wish_id = p_wish_id and wish_user_id = p_user_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_validateLogin` (IN `p_username` VARCHAR(20))  BEGIN
    select * from tbl_user where user_username = p_username;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_likes`
--

CREATE TABLE `tbl_likes` (
  `wish_id` int(11) NOT NULL,
  `like_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `wish_like` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_likes`
--

INSERT INTO `tbl_likes` (`wish_id`, `like_id`, `user_id`, `wish_like`) VALUES
(2, 1, 9, 1),
(3, 2, 9, 1),
(4, 3, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `user_username` text,
  `user_password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_username`, `user_password`) VALUES
(9, 'gopalk', 'dbzgoku12@gmail.com', 'ff3419529fb75e5baaf9f679dba06e703fe5435b76daf3d8260fd9d89d145ca5');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_wish`
--

CREATE TABLE `tbl_wish` (
  `wish_id` int(11) NOT NULL,
  `wish_title` text,
  `wish_description` text,
  `wish_user_id` int(11) DEFAULT NULL,
  `wish_date` datetime DEFAULT NULL,
  `wish_file_path` varchar(200) DEFAULT NULL,
  `wish_accomplished` int(11) DEFAULT '0',
  `wish_private` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_wish`
--

INSERT INTO `tbl_wish` (`wish_id`, `wish_title`, `wish_description`, `wish_user_id`, `wish_date`, `wish_file_path`, `wish_accomplished`, `wish_private`) VALUES
(2, 'Hello World 3!', 'This is another item in the bucket list.', 9, '2017-09-14 03:38:58', NULL, 0, 0),
(3, 'Hello World 4!', 'this is the fourth one', 9, '2017-09-15 03:30:19', NULL, 0, 0),
(4, 'Hello World 5!', 'this is the fifth one', 9, '2017-09-15 03:30:38', NULL, 0, 0),
(5, 'dvwrbb', 'beberbeb', 9, '2017-09-16 09:37:34', NULL, 0, 0),
(6, 'aavadvadv', 'vdvsdvsdvsd', 9, '2017-09-16 09:37:49', NULL, 0, 0),
(7, 'qgfrwgrf', 'shfhfh', 9, '2017-09-16 09:39:35', NULL, 0, 0),
(8, 'fhdfhdfdfhsddhd', 'sdgsgsgs', 9, '2017-09-16 09:39:43', NULL, 0, 0),
(9, 'sdgsdgs', 'sgsgsgsdg', 9, '2017-09-16 09:39:53', NULL, 0, 0),
(10, 'sdgsdgsgs', 'sdgsgsgsdg', 9, '2017-09-16 09:40:06', NULL, 0, 0),
(11, 'sgsgsgs', 'sgsgsgsd', 9, '2017-09-16 09:40:12', NULL, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_likes`
--
ALTER TABLE `tbl_likes`
  ADD PRIMARY KEY (`like_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_name` (`user_name`);

--
-- Indexes for table `tbl_wish`
--
ALTER TABLE `tbl_wish`
  ADD PRIMARY KEY (`wish_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_likes`
--
ALTER TABLE `tbl_likes`
  MODIFY `like_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_wish`
--
ALTER TABLE `tbl_wish`
  MODIFY `wish_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
